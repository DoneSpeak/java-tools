package io.gitlab.donespeak.javatool.toolprotobuf.withjsonformat;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.google.common.io.BaseEncoding;
import com.google.protobuf.ByteString;

import io.gitlab.donespeak.javatool.toolprotobuf.bean.BaseDataPojo;
import io.gitlab.donespeak.javatool.toolprotobuf.proto.DataTypeProto;

/**
 * @author Yang Guanrong
 * @date 2019/09/04 14:05
 */
public class ProtoBeanUtilsTest {

    private DataTypeProto.BaseData getBaseDataProto() {
        DataTypeProto.BaseData baseData = DataTypeProto.BaseData.newBuilder()
            .setDoubleVal(100.123D)
            .setFloatVal(12.3F)
            .setInt32Val(32)
            .setInt64Val(64)
            .setUint32Val(132)
            .setUint64Val(164)
            .setSint32Val(232)
            .setSint64Val(264)
            .setFixed32Val(332)
            .setFixed64Val(364)
            .setSfixed32Val(432)
            .setSfixed64Val(464)
            .setBoolVal(true)
            .setStringVal("ssss..tring")
            .setBytesVal(ByteString.copyFromUtf8("itsbytes"))
            .setEnumVal(DataTypeProto.Color.BLUE)
            .addReStrVal("re-item-0")
            .putMapVal("m-key", DataTypeProto.BaseData.newBuilder()
                .setStringVal("base-data")
                .build())
            .build();

        return baseData;
    }

    public BaseDataPojo getBaseDataPojo() {
        Map<String, BaseDataPojo> map = new HashMap<>();
        map.put("m-key", BaseDataPojo.builder().stringVal("base-data").build());

        BaseDataPojo baseDataPojo = BaseDataPojo.builder()
            .doubleVal(100.123D)
            .floatVal(12.3F)
            .int32Val(32)
            .int64Val(64)
            .uint32Val(132)
            .uint64Val(164)
            .sint32Val(232)
            .sint64Val(264)
            .fixed32Val(332)
            .fixed64Val(364)
            .sfixed32Val(432)
            .sfixed64Val(464)
            .boolVal(true)
            .stringVal("ssss..tring")
            .bytesVal("itsbytes")
            .enumVal(DataTypeProto.Color.BLUE.toString())
            .reStrVal(Arrays.asList("re-item-0"))
            .mapVal(map)
            .build();

        return baseDataPojo;
    }

    @Test
    public void toPojoBean() throws IOException {
        DataTypeProto.BaseData baseDataProto = getBaseDataProto();
        BaseDataPojo baseDataPojo = ProtoBeanUtils.toPojoBean(BaseDataPojo.class, baseDataProto);

        // System.out.println(new GsonBuilder().setPrettyPrinting().create().toJson(baseDataPojo));

        asserEqualsVerify(baseDataPojo, baseDataProto);
    }

    @Test
    public void toProtoBean() throws IOException {
        BaseDataPojo baseDataPojo = getBaseDataPojo();

        DataTypeProto.BaseData.Builder builder = DataTypeProto.BaseData.newBuilder();
        ProtoBeanUtils.toProtoBean(builder, baseDataPojo);
        DataTypeProto.BaseData baseDataProto = builder.build();

        // System.out.println(new GsonBuilder().setPrettyPrinting().create().toJson(baseDataPojo));
        // 不可用Gson转化Message（含有嵌套结构的，且嵌套的Message中含有嵌套结构），会栈溢出的
        // 因为Protobuf没有null值
        // System.out.println(JsonFormat.printer().print(baseDataProto));

        asserEqualsVerify(baseDataPojo, baseDataProto);
    }

    private void asserEqualsVerify(BaseDataPojo baseDataPojo, DataTypeProto.BaseData baseDataProto) {
        assertTrue((baseDataPojo == null) == (!baseDataProto.isInitialized()));
        if(baseDataPojo == null) {
            return;
        }
        assertEquals(baseDataPojo.getDoubleVal(), baseDataProto.getDoubleVal(), 0.0000001D);
        assertEquals(baseDataPojo.getFloatVal(), baseDataProto.getFloatVal(), 0.00000001D);
        assertEquals(baseDataPojo.getInt32Val(), baseDataProto.getInt32Val());
        assertEquals(baseDataPojo.getInt64Val(), baseDataProto.getInt64Val());
        assertEquals(baseDataPojo.getUint32Val(), baseDataProto.getUint32Val());
        assertEquals(baseDataPojo.getUint64Val(), baseDataProto.getUint64Val());
        assertEquals(baseDataPojo.getSint32Val(), baseDataProto.getSint32Val());
        assertEquals(baseDataPojo.getSint64Val(), baseDataProto.getSint64Val());
        assertEquals(baseDataPojo.getFixed32Val(), baseDataProto.getFixed32Val());
        assertEquals(baseDataPojo.getInt64Val(), baseDataProto.getInt64Val());
        assertEquals(baseDataPojo.isBoolVal(), baseDataProto.getBoolVal());
        assertEquals(baseDataPojo.isBoolVal(), baseDataProto.getBoolVal());
        assertEquals(baseDataPojo.getStringVal(), baseDataProto.getStringVal());
        // ByteString 转 base64 Strings
        if(baseDataPojo.getBytesVal() == null) {
            // 默认值为 ""
            assertTrue(baseDataProto.getBytesVal().isEmpty());
        } else {
            assertEquals(baseDataPojo.getBytesVal(), BaseEncoding.base64().encode(baseDataProto.getBytesVal().toByteArray()));
        }
        // Enum 转 String
        if(baseDataPojo.getEnumVal() == null) {
            // 默认值为 0
            assertEquals(DataTypeProto.Color.forNumber(0), baseDataProto.getEnumVal());
        } else {
            assertEquals(baseDataPojo.getEnumVal(), baseDataProto.getEnumVal().toString());
        }
        if(baseDataPojo.getReStrVal() == null) {
            // 默认为空列表
            assertEquals(0, baseDataProto.getReStrValList().size());
        } else {
            assertEquals(baseDataPojo.getReStrVal().size(), baseDataProto.getReStrValList().size());
            for(int i = 0; i < baseDataPojo.getReStrVal().size(); i ++) {
                assertEquals(baseDataPojo.getReStrVal().get(i), baseDataProto.getReStrValList().get(i));
            }
        }

        if(baseDataPojo.getMapVal() == null) {
            // 默认为空集合
            assertEquals(0, baseDataProto.getMapValMap().size());
        } else {
            assertEquals(baseDataPojo.getMapVal().size(), baseDataProto.getMapValMap().size());
            for(Map.Entry<String, DataTypeProto.BaseData> entry: baseDataProto.getMapValMap().entrySet()) {
                asserEqualsVerify(baseDataPojo.getMapVal().get(entry.getKey()), entry.getValue());
            }
        }
    }

    @Test
    public void testDefaultValue() {
        DataTypeProto.BaseData baseData = DataTypeProto.BaseData.newBuilder()
            .setInt32Val(0)
            .setStringVal("")
            .addAllReStrVal(new ArrayList<>())
            .setBoolVal(false)
            .setDoubleVal(3.14D)
            .build();
        // 默认值不会输出
        // double_val: 3.14
        System.out.println(baseData);
    }
}