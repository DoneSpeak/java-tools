package io.gitlab.donespeak.javatool.toolprotobuf;

import com.google.protobuf.Any;
import com.google.protobuf.ByteString;
import io.gitlab.donespeak.javatool.toolprotobuf.proto.DataTypeProto;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * @author Yang Guanrong
 * @date 2019/08/31 18:50
 */
public class ProtoJsonUtilTest {

    private DataTypeProto.BaseData getBaseData() {
        DataTypeProto.BaseData baseData = DataTypeProto.BaseData.newBuilder()
            .setDoubleVal(100.123D)
            .setFloatVal(12.3F)
            .setInt32Val(32)
            .setInt64Val(64)
            .setUint32Val(132)
            .setUint64Val(164)
            .setSint32Val(232)
            .setSint64Val(264)
            .setFixed32Val(332)
            .setFixed64Val(364)
            .setSfixed32Val(432)
            .setSfixed64Val(464)
            .setBoolVal(true)
            .setStringVal("ssss..tring")
            .setBytesVal(ByteString.copyFromUtf8("itsbytes"))
            .setEnumVal(DataTypeProto.Color.BLUE)
            .addReStrVal("re-item-0")
            .putMapVal("m-key", DataTypeProto.BaseData.newBuilder()
                .setStringVal("base-data")
                .build())
            .build();

        return baseData;
    }

    private DataTypeProto.DataWithAny getDataWithAny() {
        DataTypeProto.DataWithAny dataWithAny = DataTypeProto.DataWithAny.newBuilder()
            .setAnyVal(Any.pack(DataTypeProto.BaseData.newBuilder().setBoolVal(true).build()))
            .build();

        return dataWithAny;
    }

    private DataTypeProto.DataWithOneof getDataWithOneof() {
        DataTypeProto.DataWithOneof dataWithOneof = DataTypeProto.DataWithOneof.newBuilder()
            .setOneofInt(100)
            .setStringVal("name-oneof")
            .build();

        return dataWithOneof;
    }
    @Test
    public void toJson() throws IOException {
        DataTypeProto.BaseData baseData = getBaseData();

        String json = ProtoJsonUtil.toJson(baseData);
        System.out.println(json);
    }

    @Test
    public void toJson2() throws IOException {
        DataTypeProto.DataWithAny data = getDataWithAny();
        String json = ProtoJsonUtil.toJson(data);

        System.out.println(json);
    }

    @Test
    public void toJson3() throws IOException {
        DataTypeProto.DataWithOneof data = getDataWithOneof();

        String json = ProtoJsonUtil.toJson(data);

        System.out.println(json);

        DataTypeProto.DataWithOneof.Builder builder = DataTypeProto.DataWithOneof.newBuilder();
        ProtoJsonUtil.toProto(builder, json);
        DataTypeProto.DataWithOneof dataNew = builder.build();

        String jsonNew = ProtoJsonUtil.toJson(dataNew);

        System.out.println(jsonNew);
    }

    @Test
    public void toProto() {}
}