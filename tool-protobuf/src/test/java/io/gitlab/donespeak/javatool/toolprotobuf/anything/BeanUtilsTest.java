package io.gitlab.donespeak.javatool.toolprotobuf.anything;

import lombok.Builder;
import lombok.Data;
import org.junit.Assert;
import org.springframework.beans.BeanUtils;

/**
 * @author Yang Guanrong
 * @date 2019/09/03 21:32
 */
public class BeanUtilsTest {

    @Data
    @Builder
    public static class Person {
        private String name;
        private Student friend;
    }

    @Data
    @Builder
    public static class Student {
        private String name;
        private Person friend;
    }

    public static void main(String[] args) {
        Person person = Person.builder()
            .name("bob")
            .friend(Student.builder().name("jane").build())
            .build();

        Student student = Student.builder().build();

        BeanUtils.copyProperties(person, student);

        Assert.assertEquals(person.getName(), student.getName());
        Assert.assertNotNull(student.getFriend());
        Assert.assertEquals(person.getFriend().getName(), student.getFriend().getName());
    }
}