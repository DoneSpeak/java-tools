package io.gitlab.donespeak.javatool.toolprotobuf;

import com.google.protobuf.Any;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import io.gitlab.donespeak.javatool.toolprotobuf.proto.DataTypeProto;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class ProtoJsonUtilV2Test {

    public DataTypeProto.DataWithAny getData() {
        return DataTypeProto.DataWithAny.newBuilder()
            .setInt32Val(100)
            .setAnyVal(Any.pack(DataTypeProto.OnlyInt32.newBuilder()
                .setIntVal(3)
                .build()))
            .build();
    }

    @Test
    public void toJson() throws IOException {
        // 可以为 TypeRegistry 添加多个不同的Descriptor
        JsonFormat.TypeRegistry typeRegistry = JsonFormat.TypeRegistry.newBuilder()
            .add(DataTypeProto.BaseData.getDescriptor())
            .build();
        // usingTypeRegistry 方法会重新构建一个Printer
        JsonFormat.Printer printer = JsonFormat.printer()
            .usingTypeRegistry(typeRegistry);

        String json = printer.print(DataTypeProto.DataWithAny.newBuilder()
            .setAnyVal(
                Any.pack(
                    DataTypeProto.BaseData.newBuilder().setInt32Val(1235).build()))
            .build());

        System.out.println(json);

        DataTypeProto.DataWithAny.Builder builder = DataTypeProto.DataWithAny.newBuilder();
        ProtoJsonUtil.toProto(builder, json);

        System.out.println(builder.build());
    }

    @Test
    public void test() throws InvalidProtocolBufferException {
        JsonFormat.TypeRegistry typeRegistry = JsonFormat.TypeRegistry.newBuilder()
            .add(DataTypeProto.OnlyInt32.getDescriptor())
            .build();

        JsonFormat.Printer printer = JsonFormat.printer().usingTypeRegistry(typeRegistry);

        String json = printer.print(getData());
        System.out.println(json);
    }
}