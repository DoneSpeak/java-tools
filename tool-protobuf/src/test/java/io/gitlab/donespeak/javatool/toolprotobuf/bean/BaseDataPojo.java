package io.gitlab.donespeak.javatool.toolprotobuf.bean;

import lombok.*;

import java.util.List;
import java.util.Map;

/**
 * @author Yang Guanrong
 * @date 2019/09/03 20:46
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class BaseDataPojo {
    private double doubleVal;
    private float floatVal;
    private int int32Val;
    private long int64Val;
    private int uint32Val;
    private long uint64Val;
    private int sint32Val;
    private long sint64Val;
    private int fixed32Val;
    private long fixed64Val;
    private int sfixed32Val;
    private long sfixed64Val;
    private boolean boolVal;
    private String stringVal;
    private String bytesVal;

    private String enumVal;

    private List<String> reStrVal;
    private Map<String, BaseDataPojo> mapVal;
}
