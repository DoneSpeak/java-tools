package io.gitlab.donespeak.javatool.toolprotobuf.withbeanutils;

import io.gitlab.donespeak.javatool.toolprotobuf.bean.BaseDataPojo;
import io.gitlab.donespeak.javatool.toolprotobuf.bean.StudentPojo;
import io.gitlab.donespeak.javatool.toolprotobuf.exception.ProtoPojoConversionException;
import io.gitlab.donespeak.javatool.toolprotobuf.proto.DataTypeProto;
import io.gitlab.donespeak.javatool.toolprotobuf.proto.StudentProto;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class ProtoPojoUtilWithBeanUtilsTest {

    @Test
    public void toPojo1() throws ProtoPojoConversionException {
        StudentProto.Student student = StudentProto.Student.newBuilder()
            .setAge(15)
            .setName("Bob")
            .setDeskmate(StudentProto.Student.newBuilder().setName("jane").build())
            .build();

        StudentPojo studentPojo = ProtoPojoUtilWithBeanUtils.toPojo(StudentPojo.class, student);

        assertEquals(student.getAge(), studentPojo.getAge());
        assertEquals(student.getName(), studentPojo.getName());
        assertNotNull(studentPojo.getDeskmate());
        assertEquals(student.getDeskmate().getName(), studentPojo.getDeskmate().getName());
    }

    @Test
    public void toProto1() throws ProtoPojoConversionException {
        StudentPojo studentPojo = StudentPojo.builder()
            .age(12)
            .name("jane")
            .deskmate(StudentPojo.builder().name("bob").build())
            .build();

        StudentProto.Student.Builder builder = StudentProto.Student.newBuilder();
        ProtoPojoUtilWithBeanUtils.toProto(builder, studentPojo);

        StudentProto.Student student = builder.build();

        assertEquals(studentPojo.getAge(), student.getAge());
        assertEquals(studentPojo.getName(), student.getName());
        assertNotNull(student.getDeskmate());
        assertEquals(studentPojo.getDeskmate().getName(), student.getDeskmate().getName());
    }

    @Test
    public void toPojo2() throws ProtoPojoConversionException {
        DataTypeProto.BaseData baseData = DataTypeProto.BaseData.newBuilder()
            .setInt64Val(122L)
            .addReStrVal("addf")
            .putMapVal("book", DataTypeProto.BaseData.newBuilder().setStringVal("hahha").build())
            .setEnumVal(DataTypeProto.Color.BLUE)
            .build();

        // baseData.getReStrValList();
        // baseData.getMapValMap();
        // baseData.getEnumVal();
        // baseData.getEnumValValue();

        // 无法转化list和map，因为map和list没有setter方法
        BaseDataPojo baseDataPojo = ProtoPojoUtilWithBeanUtils.toPojo(BaseDataPojo.class, baseData);

        assertEquals(baseData.getInt64Val(), baseDataPojo.getInt64Val());
        assertEquals(baseData.getEnumVal().name(), baseDataPojo.getEnumVal());
        assertTrue(baseDataPojo.getReStrVal() != null && baseDataPojo.getReStrVal().size() == 1);
        assertTrue(baseDataPojo.getMapVal() != null && baseDataPojo.getMapVal().size() == 1);
    }

    @Test
    public void toProto2() throws ProtoPojoConversionException {

        Map<String, BaseDataPojo> mapVal = new HashMap<>();
        mapVal.put("haha", BaseDataPojo.builder().stringVal("keke").build());

        BaseDataPojo baseDataPojo = BaseDataPojo.builder()
            .int64Val(233L)
            .reStrVal(Arrays.asList("a", "b"))
            .stringVal("book")
            .mapVal(mapVal)
            .enumVal(DataTypeProto.Color.RED.name())
            .build();

        DataTypeProto.BaseData.Builder builder = DataTypeProto.BaseData.newBuilder();
        ProtoPojoUtilWithBeanUtils.toProto(builder, baseDataPojo);

        DataTypeProto.BaseData baseData = builder.build();

        assertEquals(baseDataPojo.getInt64Val(), baseData.getInt64Val());
        assertEquals(baseDataPojo.getEnumVal(), baseData.getEnumVal().name());
        assertEquals(baseDataPojo.getStringVal(), baseData.getStringVal());
        assertTrue(baseData.getReStrValList() != null && baseData.getReStrValList().size() == 1);
        assertTrue(baseData.getMapValMap() != null && baseData.getMapValMap().size() == 1);
    }
}