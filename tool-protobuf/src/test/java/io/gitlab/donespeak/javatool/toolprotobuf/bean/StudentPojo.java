package io.gitlab.donespeak.javatool.toolprotobuf.bean;

import lombok.*;

/**
 * @author Yang Guanrong
 * @date 2019/09/03 20:38
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
@Builder
public class StudentPojo {
    private int age;
    private String name;
    private StudentPojo deskmate;
}
