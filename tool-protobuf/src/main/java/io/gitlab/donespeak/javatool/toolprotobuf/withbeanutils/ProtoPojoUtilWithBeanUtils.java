package io.gitlab.donespeak.javatool.toolprotobuf.withbeanutils;

import java.lang.reflect.InvocationTargetException;

import io.gitlab.donespeak.javatool.toolprotobuf.proto.StudentProto;

import com.google.protobuf.Message;

import io.gitlab.donespeak.javatool.toolprotobuf.exception.ProtoPojoConversionException;
import org.springframework.beans.BeanUtils;

/**
 * @author Yang Guanrong
 */
public class ProtoPojoUtilWithBeanUtils {

    public static void toProto(Message.Builder destProtoBuilder, Object srcPojo) throws ProtoPojoConversionException {
        // Message 都是不可变类，没有setter方法，只能通过Builder进行setter
        try {
            BeanUtils.copyProperties(srcPojo, destProtoBuilder);
        } catch (Exception e) {
            throw new ProtoPojoConversionException(e.getMessage(), e);
        }
    }

    public static <PojoType> PojoType toPojo(Class<PojoType> destPojoKlass, Message srcMessage)
        throws ProtoPojoConversionException {
        try {
            PojoType destPojo = destPojoKlass.newInstance();
            BeanUtils.copyProperties(srcMessage, destPojo);
            return destPojo;
        } catch (Exception e) {
            throw new ProtoPojoConversionException(e.getMessage(), e);
        }
    }
}
