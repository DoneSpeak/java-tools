package io.gitlab.donespeak.javatool.toolprotobuf;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.protobuf.ByteString;
import com.google.protobuf.Descriptors;
import com.google.protobuf.Descriptors.FieldDescriptor;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.MapEntry;
import com.google.protobuf.Message;
import com.google.protobuf.Message.Builder;

import io.gitlab.donespeak.javatool.toolprotobuf.exception.ProtoPojoConversionException;

/**
 * 
 * @author Yang Guanrong
 * @date 2019/08/15
 */
public class ProtoBeanMapper {

    private static final Map<Class<?>, FieldDescriptor.JavaType> BASE_PROTO_JAVA_TYPE_MAP;
    private static final Set<String> PRIMARY_WRAP_TYPES;

    static {
        Map<Class<?>, FieldDescriptor.JavaType> javaTypeMap =
            new HashMap<Class<?>, FieldDescriptor.JavaType>();
        javaTypeMap.put(int.class, FieldDescriptor.JavaType.INT);
        javaTypeMap.put(Integer.class, FieldDescriptor.JavaType.INT);
        javaTypeMap.put(long.class, FieldDescriptor.JavaType.LONG);
        javaTypeMap.put(Long.class, FieldDescriptor.JavaType.LONG);
        javaTypeMap.put(float.class, FieldDescriptor.JavaType.FLOAT);
        javaTypeMap.put(Float.class, FieldDescriptor.JavaType.FLOAT);
        javaTypeMap.put(double.class, FieldDescriptor.JavaType.DOUBLE);
        javaTypeMap.put(Double.class, FieldDescriptor.JavaType.DOUBLE);
        javaTypeMap.put(boolean.class, FieldDescriptor.JavaType.BOOLEAN);
        javaTypeMap.put(Boolean.class, FieldDescriptor.JavaType.BOOLEAN);
        javaTypeMap.put(String.class, FieldDescriptor.JavaType.STRING);
        BASE_PROTO_JAVA_TYPE_MAP = Collections.unmodifiableMap(javaTypeMap);

        // @formatter:off
		PRIMARY_WRAP_TYPES = Collections.unmodifiableSet(new HashSet<String>(Arrays.asList(
				"java.lang.Integer",
				"java.lang.Double",
				"java.lang.Float",
				"java.lang.Long",
				"java.lang.Short",
				"java.lang.Byte",
				"java.lang.Boolean",
				"java.lang.Character",
				"java.lang.String")));
		// @formatter:on
    }

    /* ======================================= */
    public static Object toProtoBean(Builder targetBuilder, Object sourcePojoBean)
        throws ReflectiveOperationException, IntrospectionException {

        Map<String, PropertyDescriptor> propertyDescriptors = getPropertyDescriptors(sourcePojoBean.getClass());

        Descriptors.Descriptor descriptor = targetBuilder.getDescriptorForType();

        for (FieldDescriptor fieldDescriptor : descriptor.getFields()) {
            String fieldName = fieldDescriptor.getJsonName();
            PropertyDescriptor pojoPropertyDescriptor = propertyDescriptors.get(fieldName);
            if (pojoPropertyDescriptor == null || pojoPropertyDescriptor.getReadMethod() == null) {
                // 如果不含有这个属性，直接忽略，保持类原本的默认值
                // 没有getter方法，直接忽略
                continue;
            }
            setProtoFieldValue(targetBuilder, fieldDescriptor, sourcePojoBean, pojoPropertyDescriptor);
        }
        return targetBuilder.build();
    }

    private static void setProtoFieldValue(Builder targetBuilder, FieldDescriptor fieldDescriptor,
        Object sourcePojoBean, PropertyDescriptor pojoPropertyDescriptor) {
        if (pojoPropertyDescriptor.getReadMethod() == null) {
            // 没有getter方法
            return;
        }
        if (!isDescriptorsMatch(pojoPropertyDescriptor, fieldDescriptor)) {
            // 类型必须要匹配
            return;
        }
        // TODO 对复合类型（List或者Map）进行复制操作
//        setProtoCompoundFieldValue(targetBuilder, fieldDescriptor, sourcePojoBean, pojoPropertyDescriptor);
        // 对普通类型（非List或者Map）进行复制操作
//        setProtoSimpleFieldValue(targetBuilder, fieldDescriptor, sourcePojoBean, pojoPropertyDescriptor);
    }

    private static void setProtoMapFieldValue(Builder targetBuilder, FieldDescriptor fieldDescriptor,
        Object sourcePojoBean, PropertyDescriptor pojoPropertyDescriptor)
        throws ReflectiveOperationException, IntrospectionException {
        Method getter = pojoPropertyDescriptor.getReadMethod();
        // 取值，赋值
        Map<?, ?> fieldValue = (Map<?, ?>)getter.invoke(sourcePojoBean);

        Builder repeatedFieldBuilder = targetBuilder.newBuilderForField(fieldDescriptor);

        for (Map.Entry<?, ?> entry : fieldValue.entrySet()) {
            Object key = entry.getKey();
            Object value = entry.getValue();

            // TODO
//            repeatedFieldBuilder.addRepeatedField(field, value);
        }
    }

    private static void setProtoListFieldValue(Builder targetBuilder, FieldDescriptor fieldDescriptor,
        Object sourcePojoBean, PropertyDescriptor pojoPropertyDescriptor) 
            throws ReflectiveOperationException, IntrospectionException {
        Method getter = pojoPropertyDescriptor.getReadMethod();
        // 取值，赋值
        List<?> fieldValue = (List<?>)getter.invoke(sourcePojoBean);

        Builder repeatedFieldBuilder = targetBuilder.newBuilderForField(fieldDescriptor);
        Object obj = repeatedFieldBuilder.build();
        System.out.println(obj);
        for (int i = 0; i < fieldValue.size(); i++) {
            
        }
        // TODO
//        targetBuilder.setField(fieldDescriptor, protoFieldValue);
    }

    private static void setProtoEnumFieldValue(Builder targetBuilder, FieldDescriptor fieldDescriptor,
        Object sourcePojoBean, PropertyDescriptor pojoPropertyDescriptor)
        throws ReflectiveOperationException, IntrospectionException {
        Method getter = pojoPropertyDescriptor.getReadMethod();
        // 取值，赋值
        int fieldValue = (int)getter.invoke(sourcePojoBean);
        Descriptors.EnumValueDescriptor protoFieldValue = fieldDescriptor.getEnumType().findValueByNumber(fieldValue);
        targetBuilder.setField(fieldDescriptor, protoFieldValue);
    }

    private static void setProtoBytesFieldValue(Builder targetBuilder, FieldDescriptor fieldDescriptor,
        Object sourcePojoBean, PropertyDescriptor pojoPropertyDescriptor)
        throws ReflectiveOperationException, IntrospectionException {
        Method getter = pojoPropertyDescriptor.getReadMethod();
        // 取值，赋值
        byte[] fieldValue = (byte[])getter.invoke(sourcePojoBean);
        Object protoFieldValue = ByteString.copyFrom(fieldValue);
        targetBuilder.setField(fieldDescriptor, protoFieldValue);
    }

    private static void setProtoMessageFieldValue(Builder targetBuilder, FieldDescriptor fieldDescriptor,
        Object sourcePojoBean, PropertyDescriptor pojoPropertyDescriptor)
        throws ReflectiveOperationException, IntrospectionException {
        Method getter = pojoPropertyDescriptor.getReadMethod();
        // 取值，赋值
        Object fieldValue = getter.invoke(sourcePojoBean);

        Object protoFieldValue = toProtoBean(targetBuilder.getFieldBuilder(fieldDescriptor), fieldValue);
        targetBuilder.setField(fieldDescriptor, protoFieldValue);
    }

    private static void setProtoSimpleFieldValue(Builder targetBuilder, FieldDescriptor fieldDescriptor,
        Object sourcePojoBean, PropertyDescriptor pojoPropertyDescriptor) throws ReflectiveOperationException {
        Method getter = pojoPropertyDescriptor.getReadMethod();
        // 取值，赋值
        Object fieldValue = getter.invoke(sourcePojoBean);
        Object protoFieldValue = fieldValue;
        targetBuilder.setField(fieldDescriptor, protoFieldValue);
    }

    /* ======================================= */
    /**
     * 将ProtoBean转化为Pojo。
     * 
     * @param <PojoType>
     *            目标Pojo的类范类类型
     * @param targetPojoClass
     *            目标Pojo类型
     * @param sourceBuilder
     *            ProtoBean的builder，Pojo的值将取自该builder
     * @return <code>pojoClass</code> 的实体对象
     * @throws ProtoPojoConversionException
     *             操作过程发生了一些错误，一般为对类的操作所导致的异常
     */
    public static <PojoType> PojoType toPojoBean(Class<PojoType> targetPojoClass, Builder sourceBuilder)
        throws ProtoPojoConversionException {
        try {
            return toPojoBeanWrap(targetPojoClass, sourceBuilder);
        } catch (Exception e) {
            throw new ProtoPojoConversionException(e.getMessage(), e);
        }
    }

    /**
     * 将ProtoBean转化为Pojo。
     * 
     * @param <PojoType>
     *            目标Pojo的类范类类型
     * @param pojoClass
     *            目标Pojo类型
     * @param builder
     *            ProtoBean的builder，Pojo的值将取自该builder
     * @return <code>pojoClass</code> 的实体对象
     * @throws ReflectiveOperationException
     *             反射操作过程中发生异常
     * @throws IntrospectionException
     *             获取getter和setter方法过程中发生异常
     */
    private static <PojoType> PojoType toPojoBeanWrap(Class<PojoType> pojoClass, Builder builder)
        throws ReflectiveOperationException, IntrospectionException {

        PojoType pojo = pojoClass.newInstance();
        Map<String, PropertyDescriptor> propertyDescriptors = getPropertyDescriptors(pojoClass);

        Descriptors.Descriptor descriptor = builder.getDescriptorForType();

        for (FieldDescriptor fieldDescriptor : descriptor.getFields()) {
            String fieldName = fieldDescriptor.getJsonName();
            PropertyDescriptor pojoPropertyDescriptor = propertyDescriptors.get(fieldName);
            if (pojoPropertyDescriptor == null || pojoPropertyDescriptor.getWriteMethod() == null) {
                // 如果不含有这个属性，直接忽略，保持类原本的默认值
                // 没有setter方法，直接忽略
                continue;
            }
            setPojoFieldValue(pojo, pojoPropertyDescriptor, builder, fieldDescriptor);
        }
        return pojo;
    }

    /**
     * 根据类 <code>klass</code> 中的getter和setter方法生成PropertyDescriptor对象。
     * 由于getClass方法不是我们需要的getter，需要进行过滤。之后将结果封装到一个map类中进行返回， 其中的key值为getter或者setter的驼峰命名法名称。
     * 
     * @param klass
     * @return 以getter或者setter的驼峰命名法名称为key值，以<code>PropertyDescriptor</code>为value值的map对象。
     * @throws IntrospectionException
     */
    private static Map<String, PropertyDescriptor> getPropertyDescriptors(Class<?> klass)
        throws IntrospectionException {
        Map<String, PropertyDescriptor> descriptors = new HashMap<String, PropertyDescriptor>();

        BeanInfo beanInfo = Introspector.getBeanInfo(klass);
        for (PropertyDescriptor descriptor : beanInfo.getPropertyDescriptors()) {
            if (Class.class.equals(descriptor.getPropertyType())) {
                // 去除 getClass() 方法的结果
                continue;
            }
            descriptors.put(descriptor.getName(), descriptor);
        }
        return descriptors;
    }

    /**
     * 通过Builder，设置Pojo的一个属性的值。
     * 
     * @param pojo
     *            目标Pojo结果
     * @param pojoPropertyDescriptor
     *            pojo的一个字段的描述
     * @param builder
     *            protoBean的Message Builder
     * @param fieldDescriptor
     *            protoBean的一个字段描述
     * @throws ReflectiveOperationException
     * @throws IntrospectionException
     */
    private static void setPojoFieldValue(Object pojo, PropertyDescriptor pojoPropertyDescriptor,
        Builder builder, FieldDescriptor fieldDescriptor)
        throws ReflectiveOperationException, IntrospectionException {
        // 对复合类型（List或者Map）进行复制操作
        setPojoCompoundFieldValue(pojo, pojoPropertyDescriptor, builder, fieldDescriptor);
        // 对普通类型（非List或者Map）进行复制操作
        setPojoSimpleFieldValue(pojo, pojoPropertyDescriptor, builder, fieldDescriptor);
    }

    /**
     * 通过Builder，设置Pojo的一个List或者Map属性的值。
     * 
     * @param pojo
     *            目标Pojo结果
     * @param pojoPropertyDescriptor
     *            pojo的一个字段的描述
     * @param builder
     *            protoBean的Message Builder
     * @param fieldDescriptor
     *            protoBean的一个字段描述
     * @throws ReflectiveOperationException
     * @throws IntrospectionException
     */
    private static void setPojoCompoundFieldValue(Object pojo, PropertyDescriptor pojoPropertyDescriptor,
        Builder builder, FieldDescriptor fieldDescriptor)
        throws ReflectiveOperationException, IntrospectionException {

        if (!fieldDescriptor.isRepeated() || !isListOrMap(pojoPropertyDescriptor.getPropertyType())) {
            // 该方法值处理 repeated 或者 map，由于map也是repeated，所以这里只需要判断repeated即可
            return;
        }

        setPojoMapFieldValue(pojo, pojoPropertyDescriptor, builder, fieldDescriptor);
        setPojoListFieldValue(pojo, pojoPropertyDescriptor, builder, fieldDescriptor);
    }

    /**
     * 通过Builder，设置Pojo的一个Map属性的值。
     * 
     * @param pojo
     *            目标Pojo结果
     * @param pojoPropertyDescriptor
     *            pojo的一个字段的描述
     * @param builder
     *            protoBean的Message Builder
     * @param fieldDescriptor
     *            protoBean的一个字段描述
     * @throws ReflectiveOperationException
     * @throws IntrospectionException
     */
    private static void setPojoMapFieldValue(Object pojo, PropertyDescriptor pojoPropertyDescriptor,
        Builder builder, FieldDescriptor fieldDescriptor)
        throws ReflectiveOperationException, IntrospectionException {
        Class<?> paramClass = pojoPropertyDescriptor.getPropertyType();
        if (!fieldDescriptor.isMapField() || !isMap(paramClass)) {
            // 该方法值处理 map
            return;
        }
        Method setterMethod = pojoPropertyDescriptor.getWriteMethod();

        // map -> Map
        // - map 本身也是 repeated
        // - Key的类型必须为 integral 或者 string.
        // - Value in a map field cannot be a map.
        // - Map fields 不可以为 repeated.
        // - key和value的值都不允许为null
        // - Maps 对应 Java中的 java.util.Collections$UnmodifiableRandomAccessList
        // - 可通过 getXXXMap() 获取到 Map<K, V> 的结果
        // put(
        // putAll
        if (!paramClass.isAssignableFrom(Map.class) || builder.getRepeatedFieldCount(fieldDescriptor) == 0) {
            return;
        }
        // 找到Map的两个范型类型
        Class<?> keyClass = getParamGenericTypeClass(setterMethod.getParameters()[0], 0);
        Class<?> valueClass = getParamGenericTypeClass(setterMethod.getParameters()[0], 1);
        // key 和 value 只能是：基本类型的封装类型，String，自定义类型

        Object destMap = HashMap.class.newInstance();
        // addMethod 如果不存在，会直接抛出 NoSuchMethodException 异常
        Method putMethod = paramClass.getMethod("put", Object.class, Object.class);
        int count = builder.getRepeatedFieldCount(fieldDescriptor);
        if (count == 0) {
            // 无需设置，保持原来的值
            return;
        }
        // fileValue 不可能为 null
        boolean keyValueClassMatch = false;
        for (int i = 0; i < count; i++) {
            MapEntry<?, ?> object = (MapEntry<?, ?>)builder.getRepeatedField(fieldDescriptor, i);
            Object protoKey = object.getKey();
            Object protoValue = object.getValue();

            if (keyValueClassMatch || (isListOrMapGenericTypeMatch(keyClass, protoKey.getClass())
                && isListOrMapGenericTypeMatch(valueClass, protoValue.getClass()))) {
                // key 和 value的类型必须匹配
                keyValueClassMatch = true;
            } else {
                break;
            }

            Object mapKey = convertProtoMapKeyOrValue(protoKey, keyClass);
            Object mapValue = convertProtoMapKeyOrValue(protoValue, valueClass);

            putMethod.invoke(destMap, mapKey, mapValue);
        }
        if (keyValueClassMatch) {
            setterMethod.invoke(pojo, destMap);
        }
    }

    /**
     * 将Proto的Map的Key或者Value元素的值转化为Pojo的Map的Key元素或者Value元素的值
     * 
     * @param protoKeyOrValue
     *            proto的Map的Key元素或者Value元素
     * @param pojoKeyOrValueClass
     *            pojo的Map的Key元素或者Value元素的类型
     * @return Pojo的Map的Key元素或者Value元素的值
     * @throws ReflectiveOperationException
     * @throws IntrospectionException
     */
    private static Object convertProtoMapKeyOrValue(Object protoKeyOrValue, Class<?> pojoKeyOrValueClass)
        throws ReflectiveOperationException, IntrospectionException {
        Object resultValue = null;
        if (Message.class.isAssignableFrom(protoKeyOrValue.getClass())) {
            Message protoKeyMessage = (Message)protoKeyOrValue;
            resultValue = toPojoBeanWrap(pojoKeyOrValueClass, protoKeyMessage.toBuilder());
        } else if (ByteString.class.isAssignableFrom(protoKeyOrValue.getClass())) {
            // bytes 会被转化为 ByteString
            ByteString byteStringProto = (ByteString)protoKeyOrValue;
            resultValue = byteStringProto.toByteArray();
        } else {
            // 基本类型 和 String
            // enum 会被转为 Integer
            // 不用转化
            resultValue = protoKeyOrValue;
        }
        return resultValue;
    }

    /**
     * 通过Builder，设置Pojo的一个List属性的值。
     * 
     * @param pojo
     *            目标Pojo结果
     * @param pojoPropertyDescriptor
     *            pojo的一个字段的描述
     * @param builder
     *            protoBean的Message Builder
     * @param fieldDescriptor
     *            protoBean的一个字段描述
     * @throws ReflectiveOperationException
     * @throws IntrospectionException
     */
    private static void setPojoListFieldValue(Object pojo, PropertyDescriptor pojoPropertyDescriptor,
        Builder builder, FieldDescriptor fieldDescriptor)
        throws ReflectiveOperationException, IntrospectionException {
        Class<?> paramClass = pojoPropertyDescriptor.getPropertyType();
        if (!fieldDescriptor.isRepeated() || fieldDescriptor.isMapField() || !isList(paramClass)) {
            // 该方法只处理 repeated，由于map也是repeated，所以这里需要额外判断map
            return;
        }
        Method setterMethod = pojoPropertyDescriptor.getWriteMethod();

        // repeated -> List
        // add
        // addAll
        // setList(index, value)
        if (!paramClass.isAssignableFrom(List.class) || builder.getRepeatedFieldCount(fieldDescriptor) == 0) {
            return;
        }
        Class<?> listItemClass = getParamGenericTypeClass(setterMethod.getParameters()[0], 0);

        Object destList = ArrayList.class.newInstance();
        // addMethod 如果不存在，会直接抛出 NoSuchMethodException 异常
        Method addMethod = paramClass.getMethod("add", Object.class);
        boolean isItemClassMatch = false;
        // fileValue 不可能为 null
        for (int i = 0; i < builder.getRepeatedFieldCount(fieldDescriptor); i++) {
            Object fieldValue = builder.getRepeatedField(fieldDescriptor, i);
            if (isItemClassMatch || isListOrMapGenericTypeMatch(listItemClass, fieldValue.getClass())) {
                // key 和 value的类型必须匹配
                isItemClassMatch = true;
            } else {
                break;
            }
            Object listItem = convertListItemValue(fieldValue, listItemClass);

            addMethod.invoke(destList, listItem);
        }
        setterMethod.invoke(pojo, destList);
    }

    /**
     * 将Proto的List元素的值转化为Pojo的List的元素中的值
     * 
     * @param protoItemValue
     *            proto的List元素
     * @param pojoItemClass
     *            pojo的List元素的类型
     * @return Pojo的List的元素中的值
     * @throws ReflectiveOperationException
     * @throws IntrospectionException
     */
    private static Object convertListItemValue(Object protoItemValue, Class<?> pojoItemClass)
        throws ReflectiveOperationException, IntrospectionException {

        Class<?> protoItemClass = protoItemValue.getClass();
        Object listItem = null;

        if (Message.class.isAssignableFrom(protoItemClass)) {
            Message protoKeyMessage = (Message)protoItemValue;
            listItem = toPojoBeanWrap(pojoItemClass, protoKeyMessage.toBuilder());
        } else if (ByteString.class.isAssignableFrom(protoItemClass)) {
            // bytes 会被转化为 ByteString
            ByteString byteStringProto = (ByteString)protoItemValue;
            listItem = byteStringProto.toByteArray();
        } else if (Descriptors.EnumValueDescriptor.class.isAssignableFrom(protoItemClass)) {
            // enum 类型
            Descriptors.EnumValueDescriptor enumValue = (Descriptors.EnumValueDescriptor)protoItemValue;
            listItem = enumValue.getNumber();
        } else {
            // 基本类型 和 String不用特别的转化
            listItem = protoItemValue;
        }
        return listItem;
    }

    /**
     * 通过Builder，设置Pojo的一个除了List以及Map之外的属性的值。
     * 
     * @param pojo
     *            目标Pojo结果
     * @param pojoPropertyDescriptor
     *            pojo的一个字段的描述
     * @param builder
     *            protoBean的Message Builder
     * @param fieldDescriptor
     *            protoBean的一个字段描述
     * @throws ReflectiveOperationException
     * @throws IntrospectionException
     */
    private static void setPojoSimpleFieldValue(Object pojo, PropertyDescriptor pojoPropertyDescriptor,
        Builder builder, FieldDescriptor fieldDescriptor)
        throws ReflectiveOperationException, IntrospectionException {

        Class<?> paramClass = pojoPropertyDescriptor.getPropertyType();
        if (fieldDescriptor.isRepeated() || isListOrMap(paramClass)) {
            // 该方法值不处理 repeated 或者 map，由于map也是repeated，所以这里只需要判断repeated即可
            return;
        }
        Method setterMethod = pojoPropertyDescriptor.getWriteMethod();

        // 在 proto 中，值不可能为 null
        FieldDescriptor.JavaType javaType = fieldDescriptor.getJavaType();
        Object fieldValue = builder.getField(fieldDescriptor);
        switch (javaType) {
            case INT:
            case LONG:
            case FLOAT:
            case DOUBLE:
            case BOOLEAN:
            case STRING: {
                if (!simpleJavaTypeMatch(javaType, paramClass)) {
                    break;
                }
                setterMethod.invoke(pojo, fieldValue);
                break;
            }
            case BYTE_STRING: {
                // 接收 ByteString对象的参数必须是 byte[] 类型
                if (!isByteArray(paramClass)) {
                    break;
                }
                ByteString byteString = (ByteString)builder.getField(fieldDescriptor);
                byte[] bytes = byteString.toByteArray();
                setterMethod.invoke(pojo, bytes);
                break;
            }
            case ENUM: {
                // 必须用Int来接收Enum值
                if (!isInt(paramClass)) {
                    break;
                }
                Descriptors.EnumValueDescriptor enumValue =
                    (Descriptors.EnumValueDescriptor)builder.getField(fieldDescriptor);
                int enumNumber = enumValue.getNumber();
                setterMethod.invoke(pojo, enumNumber);
                break;
            }
            case MESSAGE: {
                if (!builder.hasField(fieldDescriptor)) {
                    // message 对象是需要被设置的，否则保持默认值
                    break;
                }
                Object pojoBeanField = toPojoBeanWrap(paramClass, builder.getFieldBuilder(fieldDescriptor));
                setterMethod.invoke(pojo, pojoBeanField);
                break;
            }
            default: {
                // do nothing
            }
        }
    }

    private static boolean isDescriptorsMatch(PropertyDescriptor pojoPropertyDescriptor,
        FieldDescriptor fieldDescriptor) {
        Class<?> propertyType = pojoPropertyDescriptor.getPropertyType();
        if (fieldDescriptor.isMapField()) {
            // map
            return isMap(propertyType);
        } else if (fieldDescriptor.isRepeated()) {
            // repeated
            return isList(propertyType);
        }

        FieldDescriptor.JavaType javaType = fieldDescriptor.getJavaType();
        switch (javaType) {
            case INT:
            case LONG:
            case FLOAT:
            case DOUBLE:
            case BOOLEAN:
            case STRING: {
                return simpleJavaTypeMatch(javaType, propertyType);
            }
            case BYTE_STRING: {
                // 接收 ByteString对象的参数必须是 byte[] 类型
                return isByteArray(propertyType);
            }
            case ENUM: {
                // 必须用Int来接收Enum值
                return !isInt(propertyType);
            }
            case MESSAGE: {
                // propertyType 应该是一个普通的Java对象类型
                return !Collection.class.isAssignableFrom(propertyType) && !Map.class.isAssignableFrom(propertyType)
                    && !propertyType.isArray() && !String.class.isAssignableFrom(propertyType)
                    && !propertyType.isArray() && !propertyType.isPrimitive();
            }
            default: {
                return false;
            }
        }
    }

    private static boolean isListOrMap(Class<?> klass) {
        return isList(klass) || isMap(klass);
    }

    private static boolean isList(Class<?> klass) {
        return List.class.isAssignableFrom(klass);
    }

    private static boolean isMap(Class<?> klass) {
        return Map.class.isAssignableFrom(klass);
    }

    private static boolean isInt(Class<?> klass) {
        return int.class.equals(klass) || Integer.class.equals(klass);
    }

    private static boolean isByteArray(Class<?> klass) {
        return klass.isArray() && byte.class.isAssignableFrom(klass.getComponentType());
    }

    /**
     * 判断Pojo的List或者Map的范型和ProtoBean的List或者Map范型是否能够匹配
     * 
     * @param pojoItemClass
     * @param protoItemClass
     * @return
     */
    private static boolean isListOrMapGenericTypeMatch(Class<?> pojoItemClass, Class<?> protoItemClass) {
        if (Collection.class.isAssignableFrom(pojoItemClass) || Map.class.isAssignableFrom(pojoItemClass)) {
            // 不能是Collection或者是Map
            return false;
        }
        if (isWrapType(protoItemClass.getClass().getName())) {
            // 必须先判断封装类型，因为分装类型也是Object的子类
            return isWrapType(pojoItemClass.getName());
        } else if (String.class.isAssignableFrom(protoItemClass)) {
            return String.class.isAssignableFrom(pojoItemClass);
        } else if (ByteString.class.isAssignableFrom(protoItemClass)) {
            return isByteArray(pojoItemClass);
        } else if (Descriptors.EnumValueDescriptor.class.isAssignableFrom(protoItemClass)) {
            // enum 类型
            return isInt(pojoItemClass);
        } else if (Message.class.isAssignableFrom(protoItemClass)) {
            return true;
        }
        return true;
    }

    private static boolean isWrapType(String typeName) {
        return PRIMARY_WRAP_TYPES.contains(typeName);
    }

    /**
     * 获取范型参数的类类型
     * 
     * <pre>
     * Parameter param = setterMethod.getParameters()[0];
     * param.getClass().newInstance();
     * Class<?> genericClass = getListParamGenericTypeClass(param);
     * </pre>
     * 
     * @param param
     *            范型参数
     * @param paramIndex
     *            范型参数的位置下标
     * @return
     */
    private static Class<?> getParamGenericTypeClass(Parameter param, int paramIndex) {
        ParameterizedType parameterizedType = (ParameterizedType)param.getParameterizedType();
        Type genericType = parameterizedType.getActualTypeArguments()[paramIndex];
        return (Class<?>)genericType;
    }

    private static boolean simpleJavaTypeMatch(FieldDescriptor.JavaType javaType, Class<?> klassType) {
        FieldDescriptor.JavaType javaTypeFromMap = BASE_PROTO_JAVA_TYPE_MAP.get(klassType);
        return javaType.equals(javaTypeFromMap);
    }

    public static void main(String[] args) throws Exception {
//        GeneratedMessageV3.Builder<?> builder = BookVo.BookRequest.newBuilder();
//        BookVo.BookRequest request = (BookRequest)builder.build();
//        System.out.println(request.getName());

        // Method m = TempClass.class.getMethod("isOk");
        // getMethods() 默认不访问private/protected方法
        List<String> list = new ArrayList<String>();
        Class<?> klass = list.getClass();

        if (klass.isAssignableFrom(Collection.class)) {
            Object newList = klass.newInstance();
        }
    }
}