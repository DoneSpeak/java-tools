package io.gitlab.donespeak.javatool.toolprotobuf;

import com.google.protobuf.Message;
import io.gitlab.donespeak.javatool.toolprotobuf.proto.DataTypeProto;

import java.lang.reflect.InvocationTargetException;

/**
 * @author Yang Guanrong
 * @date 2019/09/01 01:05
 */
public class JsonToProto {

    public static <T extends Message> Message.Builder toProtoBuilder(Class<T> klass, String json)
        throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Message.Builder builder = (Message.Builder)klass.getMethod("newBuilder").invoke(null);
        // TODO 实现
        return builder;
    }

    public static <T extends Message> Message toProto(Class<T> klass, String json)
        throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
       return toProtoBuilder(klass, json).build();
    }

    public static void main(String[] args)
        throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Message.Builder builder = toProtoBuilder(DataTypeProto.OnlyInt32.class, "");

        System.out.println(builder.getClass());
        System.out.println(toProto(DataTypeProto.OnlyInt32.class, "").getClass());
    }
}
