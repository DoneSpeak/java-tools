package io.gitlab.donespeak.javatool.toolprotobuf;

import com.google.protobuf.Any;
import com.google.protobuf.Message;
import com.googlecode.protobuf.format.JsonFormat;
import io.gitlab.donespeak.javatool.toolprotobuf.proto.DataTypeProto;

/**
 * @author Yang Guanrong
 * @date 2019/09/01 01:05
 */
public class ProtoToJson {
    public static String toJson(Message message) {
        return null;
    }

    public static void main(String[] args) {
        DataTypeProto.DataWithAny data = DataTypeProto.DataWithAny.newBuilder()
            .setAnyVal(Any.pack(DataTypeProto.OnlyInt32.newBuilder().setIntVal(100).build()))
            .putMapVal("mapkey", DataTypeProto.BaseData.newBuilder().setStringVal("message").build())
            .build();

        JsonFormat jsonFormat = new JsonFormat();
        String json = jsonFormat.printToString(data);

        System.out.println(json);
    }
}