 package io.gitlab.donespeak.javatool.toolprotobuf;

import java.io.IOException;
import java.util.*;

import com.google.protobuf.Any;
import com.google.protobuf.Descriptors;
import com.google.protobuf.Message;
import com.google.protobuf.util.JsonFormat;
import io.gitlab.donespeak.javatool.toolprotobuf.proto.DataTypeProto;

 /**
 * 解决 Any 的问题
 * @author yangguanrong
 * @date 2019/08/30
 */
public class ProtoJsonUtilV2 {

    public static String toJson(Message sourceMessage)
            throws IOException {
        JsonFormat.TypeRegistry typeRegistry = JsonFormat.TypeRegistry.newBuilder()
            .add(sourceMessage.getDescriptorForType())
            .build();

        JsonFormat.Printer printer = JsonFormat.printer();
        // 添加类型注册器
        printer.usingTypeRegistry(typeRegistry);

        String json = printer.print(sourceMessage);
        return json;
    }

     /**
      *
      * 一旦涉及到Map和List这样的范型，都会变得非常得麻烦
      * 还要判断一个域是否为Any的
      * @param sourceMessage
      * @return
      */
    private static Set<Descriptors.Descriptor> parseAllAnyField(Message sourceMessage) {
        Set<Descriptors.Descriptor> descriptors = new HashSet<>();
        parseAllAnyField(sourceMessage, descriptors);
        return descriptors;
    }

    //TODO 参数 Descriptors.Descriptor descriptorForType 是否有必要有方法使用message进行代替，这样的话才会简单一点
     private static void parseAllAnyField(Message sourceMessage, Set<Descriptors.Descriptor> descriptors) {

         for(Descriptors.FieldDescriptor fieldDescriptor: sourceMessage.getDescriptorForType().getFields()) {

             if(fieldDescriptor.getType() != Descriptors.FieldDescriptor.Type.MESSAGE) {
                 // Any 字段只可能是 Message
                 // massage 类型还可能是 repeated 或者 map
                continue;
             }
             if(fieldDescriptor.isMapField()) {
                 // map 也是repeated，必须在repeated判断之前
                 // TODO 对key 和 value 进行判断
             } else if(fieldDescriptor.isRepeated()) {
                // TODO 先对类判断，是否存在Any，如存在则对repeated的每个元素都判断，否则直接跳过
             } else if(isAnyTypeField(fieldDescriptor) && sourceMessage.hasField(fieldDescriptor)) {

                 Message anyVal = ((Message)sourceMessage.getField(fieldDescriptor));
                 Descriptors.Descriptor descriptor = anyVal.getDescriptorForType();
                 System.out.println("### " + anyVal);
                 if(descriptors.contains(descriptor)) {
                     // 防止重复添加
                    continue;
                 }
                 // 把Any转化为message的Descriptor
                 descriptors.add(descriptor);
                 // Any 属性的Message中也可能含有 Any
                 parseAllAnyField(anyVal, descriptors);
             }
         }
     }

    private static boolean isAnyTypeField(Descriptors.FieldDescriptor fieldDescriptor) {
        return (fieldDescriptor.getType() == Descriptors.FieldDescriptor.Type.MESSAGE)
            && (fieldDescriptor.getMessageType().getFullName().equals(Any.getDescriptor().getFullName()));
    }

    public static Message toProtoBean(Message.Builder targetBuilder, String json) throws IOException {
        JsonFormat.parser().merge(json, targetBuilder);
        return targetBuilder.build();
    }

    public static void main(String[] args) {
        System.out.println(Any.getDescriptor().getFullName());

        for(Descriptors.Descriptor descriptor: parseAllAnyField(DataTypeProto.DataWithAny.newBuilder()
                .setAnyVal(Any.pack(DataTypeProto.BaseData.newBuilder().setStringVal("dhahah").build())).build())) {
            System.out.println(descriptor.getFullName());
        }
    }
}
