package io.gitlab.donespeak.javatool.toolprotobuf.exception;

import java.io.IOException;

/**
 * @author Yang Guanrong
 */
public class ProtoJsonConversionException extends IOException {

	private static final long serialVersionUID = -2762237060624381263L;

	public ProtoJsonConversionException() {
		super();
	}

	public ProtoJsonConversionException(String message) {
		super(message);
	}

	public ProtoJsonConversionException(Throwable cause) {
		super(cause);
	}

	public ProtoJsonConversionException(String message, Throwable cause) {
		super(message, cause);
	}
}
