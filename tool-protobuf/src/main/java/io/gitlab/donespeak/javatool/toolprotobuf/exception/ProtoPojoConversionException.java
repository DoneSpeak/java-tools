package io.gitlab.donespeak.javatool.toolprotobuf.exception;

public class ProtoPojoConversionException extends Exception {

	private static final long serialVersionUID = -2762237060624381263L;
	
	public ProtoPojoConversionException() {
		super();
	}
	
	public ProtoPojoConversionException(String message) {
		super(message);
	}
	
	public ProtoPojoConversionException(Throwable cause) {
		super(cause);
	}
	
	public ProtoPojoConversionException(String message, Throwable cause) {
		super(message, cause);
	}
}
