package io.gitlab.donespeak.javatool.toolprotobuf;

import com.google.protobuf.Message;

import io.gitlab.donespeak.javatool.toolprotobuf.exception.ProtoPojoConversionException;

public interface ProtoPojoUtil {
    
    <ProtoType> Message toProto(Class<ProtoType> protoKlass, Object pojo) throws ProtoPojoConversionException;
    
    <PojoType> PojoType toPojo(Class<PojoType> pojoKlass, Message message) throws ProtoPojoConversionException;
}