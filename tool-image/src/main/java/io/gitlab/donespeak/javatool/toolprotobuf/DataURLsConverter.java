 package io.gitlab.donespeak.javatool.toolprotobuf;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * 参考：
 * <ul>
 *   <li>[riversun/ToDataURI.java](https://gist.github.com/riversun/9694fbda50e91a7059675eb9bf75839c)</li>
 * </ul>
 * @author Guanrong Yang
 * @date 2019/06/26
 */
public class DataURLsConverter {

    public static String toBase64DataURL(File file) throws IOException {        
        String contentType = Files.probeContentType(file.toPath());
        return "data:" + contentType + ";base64,";
    }
}